package usecase

import (
	"database/sql"
	"empoyee/model/request"
	"empoyee/repository"
	"errors"
)

type usecaseOffice struct {
	auth         Auth
	Office       repository.OfficeRepo
	RepoEmployee repository.EmployeeRepo
}

func NewUsecaseOffice(auth Auth, Office repository.OfficeRepo, Employee repository.EmployeeRepo) UsecaseOffice {
	return &usecaseOffice{auth, Office, Employee}
}

func (u *usecaseOffice) InsertOffice(req *request.Office) error {
	err := u.Office.AddKantor(req)
	if err != nil {
		return err
	}
	return nil
}

func (u *usecaseOffice) UpdateJadwal(req *request.Office) error {
	err := u.Office.UpdateJadwal(req.Nama, req.ClockIn, req.ClockOut)
	if err != nil {
		return err
	}
	return nil
}

func (u *usecaseOffice) Absensi(employeeRequest *request.Jadwal) error {
	err := u.RepoEmployee.MasukKantor(employeeRequest)
	if err != nil {
		return err
	}
	return nil
}

func (u *usecaseOffice) GetByEmployeeId(employeeID string) (res *request.EmployeeRequest, err error) {
	res, err = u.RepoEmployee.GetUsersByEmplyeeId(employeeID)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (u *usecaseOffice) AddEmployee(employeeRequest *request.EmployeeRequest) error {
	if err := u.RepoEmployee.AddEmploye(employeeRequest); err != nil {
		return err
	}
	return nil

}

func (u *usecaseOffice) Login(employeeRequest *request.EmployeeRequest) (string, error) {
	res, err := u.RepoEmployee.GetUsersByEmail(employeeRequest.Email, employeeRequest.Password)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", errors.New("email anda tidak ditemukan")
		}
		return "", err
	}
	if res.No == "" {
		return "", errors.New("password atau email salah")
	}

	Token, err := u.auth.GenerateTokenJWT(res.No)
	if err != nil {
		return Token, err
	}

	return Token, nil

}

func (u *usecaseOffice) UploadFoto(Path, NoId string) error {
	if err := u.RepoEmployee.UpdateFotoById(Path, NoId); err != nil {
		return err
	}
	return nil

}

func (u *usecaseOffice) ProfileOffices(employeeID string) (res *request.Office, err error) {
	res, err = u.Office.GetProfileOffice(employeeID)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (u *usecaseOffice) TimeSheetEmplyee(employeeName, officeName string) (res *request.Jadwal, err error) {
	res, err = u.Office.TimeSheetEmployee(employeeName, officeName)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (u *usecaseOffice) AbsensiPulang(NamaEmployee, TimeIn, timeOut string) error {
	err := u.RepoEmployee.PulangKantor(NamaEmployee, TimeIn, timeOut)
	if err != nil {
		return err
	}
	return nil
}
