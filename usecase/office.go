package usecase

import "empoyee/model/request"

type UsecaseOffice interface {
	AddEmployee(employeeRequest *request.EmployeeRequest) error
	InsertOffice(req *request.Office) error
	Login(employeeRequest *request.EmployeeRequest) (string, error)
	UploadFoto(Path, NoId string) error
	Absensi(employeeRequest *request.Jadwal) error
	AbsensiPulang(NamaEmployee, TimeIn, timeOut string) error
	UpdateJadwal(req *request.Office) error
	ProfileOffices(employeeID string) (res *request.Office, err error)
	GetByEmployeeId(employeeID string) (res *request.EmployeeRequest, err error)
	TimeSheetEmplyee(employeeName, officeName string) (res *request.Jadwal, err error)
}
