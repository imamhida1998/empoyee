package controller

import (
	"empoyee/usecase"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

type AuthJwt struct {
	authjwt usecase.Auth
}

func NewJWTHandler(service usecase.Auth) AuthJwt {
	return AuthJwt{service}
}

func (j *AuthJwt) MiddlewareJWT(c *gin.Context) {
	authHeader := c.GetHeader("Authorization")
	if !strings.Contains(authHeader, "Bearer") {
		//	response := helpers.APIResponse(http.StatusUnauthorized, "Unauthorized", "error", nil)
		c.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
		return
	}

	var tokenString string
	arrayToken := strings.Split(authHeader, " ")
	if len(arrayToken) == 2 {
		tokenString = arrayToken[1]
	}

	token, err := j.authjwt.ValidateToken(tokenString)
	if err != nil {
		//response := helpers.APIResponse(http.StatusUnauthorized, "Unauthorized", "error", err)
		c.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
		return
	}

	Claims, ok := token.Claims.(jwt.MapClaims)

	fmt.Println("detail account: ", Claims["no_id"])
	if !ok || !token.Valid {
		//	response := helpers.APIResponse(http.StatusUnauthorized, "Unauthorized", "error", nil)
		c.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
		return

	}

	NoId := Claims["no_id"].(string)

	c.Set("no_id", NoId)
	c.Next()
}
