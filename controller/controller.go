package controller

import (
	"empoyee/model/request"
	"empoyee/usecase"
	"empoyee/utils"
	"fmt"
	"image/jpeg"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

type Controller struct {
	auth     AuthJwt
	services usecase.UsecaseOffice
}

func NewController(auth AuthJwt, services usecase.UsecaseOffice) Controller {
	return Controller{auth, services}

}

func (c *Controller) Route(app *gin.Engine) {
	app.POST("/login", c.Login)
	app.POST("/add-employee", c.AddEmployee)
	empoye := app.Group("/employee")
	empoye.Use(c.auth.MiddlewareJWT)
	empoye.POST("/avatar", c.UploadAvatar)
	empoye.POST("/absensi-masuk", c.AbsensiMasuk)
	empoye.POST("/absensi-keluar", c.AbsensiPulang)
	office := app.Group("/office")
	office.POST("/add-office", c.AddOffice)
	office.PUT("/jadwal", c.UploadJadwalKantor)

}

func (c *Controller) AddEmployee(ctx *gin.Context) {
	var requestData request.EmployeeRequest
	err := ctx.BindJSON(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	err = c.services.AddEmployee(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	response := map[string]any{
		"status":  201,
		"message": "succes",
	}
	ctx.JSON(http.StatusCreated, response)

}

func (c *Controller) AddOffice(ctx *gin.Context) {
	var requestData request.Office
	err := ctx.BindJSON(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	err = c.services.InsertOffice(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	response := map[string]any{
		"status":  201,
		"message": "succes",
	}
	ctx.JSON(http.StatusCreated, response)

}

func (c *Controller) Login(ctx *gin.Context) {
	var requestData request.EmployeeRequest
	err := ctx.BindJSON(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	Token, err := c.services.Login(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}
	response := map[string]any{
		"status":  200,
		"message": "succes",
		"access": map[string]any{
			"token": Token,
		},
	}
	ctx.JSON(http.StatusOK, response)

}

func (c *Controller) UploadAvatar(ctx *gin.Context) {
	file, err := ctx.FormFile("avatar")
	if err != nil {
		data := gin.H{"is_uploaded": false}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	NoId := ctx.MustGet("no_id").(string)

	userResult, err := c.services.GetByEmployeeId(NoId)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}
	if NoId != userResult.No {
		data := map[string]any{
			"status":  401,
			"message": "token invalid",
		}
		ctx.JSON(http.StatusUnauthorized, data)
		return
	}

	path := "assets/employee/" + file.Filename

	err = ctx.SaveUploadedFile(file, path)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	c.services.UploadFoto(path, NoId)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	response := map[string]any{
		"status":      200,
		"is_uploaded": true,
		"message":     "succes",
	}
	ctx.JSON(http.StatusOK, response)

}

func (c *Controller) AbsensiMasuk(ctx *gin.Context) {

	timeNow := time.Now()
	file, _, err := ctx.Request.FormFile("avatar")
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	NoId := ctx.MustGet("no_id").(string)

	userResult, err := c.services.GetByEmployeeId(NoId)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	if userResult.PhotoURL == "" {
		data := map[string]any{
			"status":  400,
			"message": "mohon upload avatar anda terlebih dahulu untuk melakukan absensi",
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	officeResult, err := c.services.ProfileOffices(userResult.Kantor)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": "user tsb tidak terdaftar dikantor ini",
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	img1, err := os.Open(userResult.PhotoURL)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}
	images1, err := jpeg.Decode(img1)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	images2, err := jpeg.Decode(file)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}
	ssim := utils.CompareImages(images1, images2)

	if ssim < 80 {
		data := map[string]any{
			"status":  http.StatusNotAcceptable,
			"message": "similiarity under 80 %",
		}
		ctx.JSON(http.StatusNotAcceptable, data)
		return
	}

	if NoId != userResult.No && officeResult.Nama != userResult.Kantor {
		data := map[string]any{
			"status":  401,
			"message": "anda tidak terdaftar di kantor ini",
		}
		ctx.JSON(http.StatusUnauthorized, data)
		return
	}

	timein, err := time.Parse("15:04", officeResult.ClockIn)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}
	timenow, err := time.Parse("15:04", timeNow.Format("15:04"))
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	loc, _ := time.LoadLocation("Asia/Jakarta")
	message := fmt.Sprintf("Maaf %s,anda tidak boleh masuk karena terlambat", userResult.Nama)
	if !timenow.In(loc).Before(timein) {
		data := map[string]any{
			"status":  401,
			"message": message,
		}
		ctx.JSON(http.StatusUnauthorized, data)
		return

	}

	AbsensiReq := &request.Jadwal{
		Nama:   userResult.Nama,
		Kantor: officeResult.Nama,
		TimeIn: timeNow.Format("15:04"),
	}
	err = c.services.Absensi(AbsensiReq)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	response := map[string]any{
		"similarity": ssim,
		"status":     200,
		"message":    "succes",
	}
	ctx.JSON(http.StatusOK, response)

}

func (c *Controller) UploadJadwalKantor(ctx *gin.Context) {
	var requestData request.Office
	err := ctx.BindJSON(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	err = c.services.UpdateJadwal(&requestData)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	response := map[string]any{
		"status":  200,
		"message": "succes",
	}
	ctx.JSON(http.StatusOK, response)

}

func (c *Controller) AbsensiPulang(ctx *gin.Context) {
	timeNow := time.Now()
	NoId := ctx.MustGet("no_id").(string)

	userResult, err := c.services.GetByEmployeeId(NoId)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	if userResult.No == "" {
		data := map[string]any{
			"status":  400,
			"message": "data karyawan tidak ditemukan",
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	officeResult, err := c.services.ProfileOffices(userResult.Kantor)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": "user tsb tidak terdaftar dikantor ini",
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	userTimeSHeet, err := c.services.TimeSheetEmplyee(userResult.Nama, userResult.Kantor)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": "user tsb tidak terdaftar dikantor ini",
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	if NoId != userResult.No && officeResult.Nama != userResult.Kantor {
		data := map[string]any{
			"status":  401,
			"message": "anda tidak terdaftar di kantor ini",
		}
		ctx.JSON(http.StatusUnauthorized, data)
		return
	}

	timeout, err := time.Parse("15:04", officeResult.ClockOut)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}
	timenow, err := time.Parse("15:04", timeNow.Format("15:04"))
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}
	loc, _ := time.LoadLocation("Asia/Jakarta")
	message := fmt.Sprintf("maaf %s, anda belum waktunya pulang", userResult.Nama)
	if !timenow.In(loc).After(timeout) {
		data := map[string]any{
			"status":  401,
			"message": message,
		}
		ctx.JSON(http.StatusUnauthorized, data)
		return

	}

	userTimeSHeet, err = c.services.TimeSheetEmplyee(userResult.Nama, userResult.Kantor)
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": "user tsb tidak terdaftar dikantor ini",
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	err = c.services.AbsensiPulang(userResult.Nama, userTimeSHeet.TimeIn, timeNow.Format("15:04"))
	if err != nil {
		data := map[string]any{
			"status":  400,
			"message": err.Error(),
		}
		ctx.JSON(http.StatusBadRequest, data)
		return
	}

	response := map[string]any{
		"status":  200,
		"message": "succes",
	}
	ctx.JSON(http.StatusOK, response)

}
