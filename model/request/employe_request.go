package request

type EmployeeRequest struct {
	No       string `json:"employee_id,omitempty"`
	Nama     string `json:"nama,omitempty"`
	Kantor   string `json:"kantor,omitempty"`
	Email    string `json:"email"`
	Password string `json:"password"`
	PhotoURL string `json:"photo_url,omitempty"`
}
