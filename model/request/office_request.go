package request

type Jadwal struct {
	No      string `json:"no,omitempty"`
	Kantor  string `json:"kantor,omitempty"`
	Nama    string `json:"nama"`
	TimeIn  string `json:"time_in,omitempty"`
	TimeOut string `json:"time_out,omitempty"`
}

type Office struct {
	NoOffice    string `json:"no_office,omitempty"`
	Nama        string `json:"nama"`
	ClockIn     string `json:"clock_in"`
	ClockOut    string `json:"clock_out"`
	URLEmployee string `json:"url_employee,omitempty"`
}
