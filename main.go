package main

import (
	"empoyee/config"
	"empoyee/controller"
	"empoyee/lib/db"
	"empoyee/repository"
	"empoyee/usecase"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	route := gin.Default()
	set := config.Config{}
	set.CatchError(set.InitEnv())
	Database := set.GetDBConfig()
	db, err := db.ConnectiontoMYSQL(Database)
	if err != nil {
		log.Println(err)
		return
	}

	RepoEmployee := repository.NewRepoEmployee(db)
	RepoOffice := repository.NewRepoOffice(db)

	Auth := usecase.NewJWTService()
	UsecaseOffice := usecase.NewUsecaseOffice(Auth, RepoOffice, RepoEmployee)

	Auths := controller.NewJWTHandler(Auth)
	controller := controller.NewController(Auths, UsecaseOffice)

	controller.Route(route)

	route.Run("127.0.0.1:7001")

}
