package repository

import (
	"database/sql"
	"empoyee/model/request"
	"errors"

	"github.com/google/uuid"
)

type employeRepo struct {
	db *sql.DB
}

func NewRepoEmployee(db *sql.DB) EmployeeRepo {
	return &employeRepo{db}
}

func (e *employeRepo) AddEmploye(params *request.EmployeeRequest) error {
	uuID := uuid.NewString()
	query := `insert into employee (employee_id,nama,kantor,email,password,created_at)values (?,?,?,?,?,NOW())`

	_, err := e.db.Exec(query, uuID, params.Nama, params.Kantor, params.Email, params.Password)
	if err != nil {
		return err
	}
	return nil

}

func (e *employeRepo) MasukKantor(params *request.Jadwal) error {
	params.No = uuid.NewString()
	query := `insert into jadwal (no,kantor,nama,time_in,time_out,created_at)values (?,?,?,?,?,NOW())`

	_, err := e.db.Exec(query, params.No, params.Kantor, params.Nama, params.TimeIn, params.TimeOut)
	if err != nil {
		return err
	}
	return nil

}
func (e *employeRepo) GetUsersByEmail(email, password string) (res *request.EmployeeRequest, err error) {
	query := `select employee_id,nama,kantor,password,email,photo_url  from employee where email = ? and password = ?`
	rows, err := e.db.Query(query, email, password)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return res, nil
		}
		return res, err
	}
	var result request.EmployeeRequest
	var PhotoUrl sql.NullString
	for rows.Next() {

		err = rows.Scan(
			&result.No,
			&result.Nama,
			&result.Kantor,
			&result.Password,
			&result.Email,
			&PhotoUrl)
		if !PhotoUrl.Valid {
			result.PhotoURL = ""
		}

		if err != nil {
			return nil, err
		}

	}
	return &result, nil

}

func (e *employeRepo) GetUsersByEmplyeeId(EmployeeId string) (res *request.EmployeeRequest, err error) {
	query := `select employee_id,nama,kantor,password,email,photo_url  from employee where employee_id = ? `
	rows, err := e.db.Query(query, EmployeeId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return res, nil
		}
		return res, err
	}
	var result request.EmployeeRequest
	var PhotoUrl sql.NullString
	for rows.Next() {

		err = rows.Scan(
			&result.No,
			&result.Nama,
			&result.Kantor,
			&result.Password,
			&result.Email,
			&PhotoUrl)
		if !PhotoUrl.Valid {
			result.PhotoURL = ""
		} else {
			result.PhotoURL = PhotoUrl.String
		}

		if err != nil {
			return nil, err
		}

	}
	return &result, nil

}

func (e *employeRepo) UpdateFotoById(photoUrl, NoID string) error {
	query := `update employee set photo_url = ? where employee_id = ?`
	if _, err := e.db.Exec(query, photoUrl, NoID); err != nil {
		return err
	}
	return nil

}

func (e *employeRepo) PulangKantor(Nama, timeIn, timeOut string) error {

	query := `update jadwal set time_out = ? where  nama = ? and time_in = ?`

	_, err := e.db.Exec(query, timeOut, Nama, timeIn)
	if err != nil {
		return err
	}
	return nil

}
