package repository

import (
	"database/sql"
	"empoyee/model/request"
	"errors"

	"github.com/google/uuid"
)

type officeRepo struct {
	db *sql.DB
}

func NewRepoOffice(db *sql.DB) OfficeRepo {
	return &officeRepo{db}
}

func (o *officeRepo) AddKantor(params *request.Office) error {
	uuID := uuid.NewString()
	query := `insert into office (office_id,nama,clock_in,clock_out,created_at)values (?,?,?,?,NOW())`

	_, err := o.db.Exec(query, uuID, params.Nama, params.ClockIn, params.ClockOut)
	if err != nil {
		return err
	}
	return nil
}

func (o *officeRepo) UpdateJadwal(Kantor, ClockIn, ClockOut string) error {
	query := `update 
    				office 
				set
				    clock_in= ? ,
				    clock_out=?	
				where 
					nama = ?`
	if _, err := o.db.Exec(query, ClockIn, ClockOut, Kantor); err != nil {
		return err
	}
	return nil
}

func (o *officeRepo) GetProfileOffice(NamaKantor string) (res *request.Office, err error) {
	query := `select office_id,nama,clock_in,clock_out from office where nama = ? `
	rows, err := o.db.Query(query, NamaKantor)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return res, nil
		}
		return res, err
	}
	var result request.Office
	//var PhotoUrl sql.NullString
	for rows.Next() {

		err = rows.Scan(
			&result.NoOffice,
			&result.Nama,
			&result.ClockIn,
			&result.ClockOut)
		//if !PhotoUrl.Valid {
		//	result.PhotoURL = ""
		//} else {
		//	result.PhotoURL = PhotoUrl.String
		//}

		if err != nil {
			return nil, err
		}

	}
	return &result, nil

}
func (o *officeRepo) TimeSheetEmployee(employeeName, officeName string) (res *request.Jadwal, err error) {
	query := `select no,kantor,nama,time_in,time_out from jadwal where nama = ? and kantor = ? `
	rows, err := o.db.Query(query, employeeName, officeName)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return res, nil
		}
		return res, err
	}
	var result request.Jadwal
	//var PhotoUrl sql.NullString
	for rows.Next() {

		err = rows.Scan(
			&result.No,
			&result.Kantor,
			&result.Nama,
			&result.TimeIn,
			&result.TimeOut)
		if err != nil {
			return nil, err
		}

	}
	return &result, nil

}
