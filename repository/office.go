package repository

import "empoyee/model/request"

type OfficeRepo interface {
	AddKantor(params *request.Office) error
	UpdateJadwal(Kantor, ClockIn, ClockOut string) error
	GetProfileOffice(NamaKantor string) (res *request.Office, err error)
	TimeSheetEmployee(employeeName, officeName string) (res *request.Jadwal, err error)
}
