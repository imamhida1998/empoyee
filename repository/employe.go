package repository

import "empoyee/model/request"

type EmployeeRepo interface {
	AddEmploye(params *request.EmployeeRequest) error
	MasukKantor(params *request.Jadwal) error
	PulangKantor(Nama, timeIn, timeOut string) error
	UpdateFotoById(photoUrl, NoID string) error
	GetUsersByEmail(email, password string) (res *request.EmployeeRequest, err error)
	GetUsersByEmplyeeId(EmployeeId string) (res *request.EmployeeRequest, err error)
}
