package utils

import (
	"image"
	"math"
)

func CompareImages(img1, img2 image.Image) int {
	bounds := img1.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y

	var diffSum float64
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			r1, g1, b1, _ := img1.At(x, y).RGBA()
			r2, g2, b2, _ := img2.At(x, y).RGBA()

			diffSum += math.Abs(float64(r1-r2)/65535.0) +
				math.Abs(float64(g1-g2)/65535.0) +
				math.Abs(float64(b1-b2)/65535.0)
		}
	}

	pixelCount := width * height * 3
	meanDiff := diffSum / float64(pixelCount)
	similarity := int((1.0 - meanDiff) * 100)

	return similarity
}
